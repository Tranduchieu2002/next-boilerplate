module.exports = {
  apps: [
    {
      name: "web-frontend",
      script: "yarn",
      args: "start",
      instances: 1,
      autorestart: true,
      watch: true,
      max_memory_restart: "8G",
      env: {
        NODE_ENV: "development",
      },
    },
  ],
};